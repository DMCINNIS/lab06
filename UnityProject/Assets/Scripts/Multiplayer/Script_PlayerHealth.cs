﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Script_PlayerHealth : NetworkBehaviour {



    [SyncVar]
    int syncHealth;

    int currentHealth = 0, health = 100;
    
    
    //add text decleration  

   // Use this for initialization
   void Start () {
        
        if(isLocalPlayer)
        {
            //set Player Name text to name of this game object          
            
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (currentHealth != health)
        {
            currentHealth = health;
            TransmitHealth();
        }

        StartCoroutine("DisplayHealth");
	
	}

    [Client]
    void TransmitHealth()
    {

        CmdSendHealthToServer(currentHealth);
        
    }

    [Command]
    void CmdSendHealthToServer(int health)
    {
        syncHealth = health;
        Debug.Log("Current Health is at: " + syncHealth.ToString());
    }

    IEnumerator DisplayHealth()
    {
        TransmitHealth();

        yield return new WaitForSeconds(2.0f);
    }

    void _TakeDamage()
    {
        if (health > 0)
        {
            health -= 10;
        }
    }

    void _TakeHealing()
    {
        if (health < 100)
        {
            health += 10;
        }

    }

}
